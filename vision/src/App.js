import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { getRecipes } from './actions/recipe.actions';
import { Button } from 'mdbreact';
import RecipeList from './Components/CardList'
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import './styles.css';
import SideDrawer from './Components/SideDrawer';
import './style5.css';

class App extends Component {
  state={
    toggle: true,
  }

  handleClick = () =>{
    this.props.getRecipes();
  }

  onClickToggleHandler =()=>{
    this.setState({toggle: !this.state.toggle})
  }
  render() {
    return (

      <div className="App">
          <SideDrawer open = {this.state.toggle} toggleHandler={this.onClickToggleHandler}/>
          <RecipeList />
        
      </div>
    );
  }
}

//<RecipePage/>
const mapStateToProps = (state) => {
  return {
    recipeState: state.recipes
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getRecipes: () => dispatch(getRecipes())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
