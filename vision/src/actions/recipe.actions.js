import axios from 'axios';

export const GET_RECIPE_PENDING = 'GET_RECIPE_PENDING';
export const GET_RECIPE_SUCCESS = 'GET_RECIPE_SUCCESS';
export const GET_RECIPE_ERROR = 'GET_RECIPE_ERROR';
export const GET_RECIPE_DETAIL_PENDING = 'GET_RECIPE_DETAIL_PENDING'
export const GET_RECIPE_DETAIL_ERROR = 'GET_RECIPE_DETAIL_ERROR'
export const GET_RECIPE_DETAIL_SUCCESS ='GET_RECIPE_DETAIL_SUCCESS'
 
const ROOT_URL = 'http://localhost:8080/getRecipesByIngr';
const ROOT_URL2 = 'http://localhost:8080/getRecipeDetails';

function setGetRecipePending(isGetRecipePending) {
  return {
    type: GET_RECIPE_PENDING,
    isGetRecipePending: isGetRecipePending
  };
}

function setGetRecipeSuccess(isGetRecipeSuccess, recipes) {
  return {
    type: GET_RECIPE_SUCCESS,
    isGetRecipeSuccess: isGetRecipeSuccess,
	recipes
  };
}

function setGetRecipeError(getGetRecipeError) {
  return {
    type: GET_RECIPE_ERROR,
    getGetRecipeError: getGetRecipeError
  }
}

export function getRecipes(query, calorieRange, time) {
	return dispatch => {
		dispatch(setGetRecipePending(true));
		dispatch(setGetRecipeSuccess(false));
		dispatch(setGetRecipeError(null));

		axios.get(ROOT_URL, {
            params:{
                ingr: ['fish sauce'].join(',')
            }
		})
		.then(res => {
            console.log(res)
            dispatch(setGetRecipePending(false));
            dispatch(setGetRecipeSuccess(true,res.data));
		})
		.catch(err => {
			dispatch(setGetRecipePending(false));
			dispatch(setGetRecipeSuccess(false,null));
			dispatch(setGetRecipeError(err.response));
		});
	}
}

function setGetRecipeDetailsPending(isGetRecipePending) {
    return {
      type: GET_RECIPE_DETAIL_PENDING,
      isGetRecipeDetailPending: isGetRecipePending
    };
  }
  
  function setGetRecipeDetailsSuccess(isGetRecipeSuccess, recipes) {
    return {
      type: GET_RECIPE_DETAIL_SUCCESS,
      isGetRecipeDetailSuccess: isGetRecipeSuccess,
      selectedRecipe: recipes
    };
  }
  
  function setGetRecipeDetailsError(getGetRecipeError) {
    return {
      type: GET_RECIPE_DETAIL_ERROR,
      getGetRecipeError: getGetRecipeError
    }
  }
  
  export function getRecipeDetails(id) {
      return dispatch => {
          dispatch(setGetRecipeDetailsPending(true));
          dispatch(setGetRecipeDetailsSuccess(false));
          dispatch(setGetRecipeDetailsError(null));
  
          axios.get(ROOT_URL2, {
                params:{
                    id: id
                }
          })
          .then(res => {
              console.log(res)
              dispatch(setGetRecipeDetailsPending(false));
              dispatch(setGetRecipeDetailsSuccess(true,res.data));
          })
          .catch(err => {
              dispatch(setGetRecipeDetailsPending(false));
              dispatch(setGetRecipeDetailsSuccess(false,null));
              dispatch(setGetRecipeDetailsError(err.response));
          });
      }
  }
