import { combineReducers } from 'redux';
import recipeReducer from './recipe.reducer';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const rootPersistConfig = {
	key: 'root',
	storage,
	blacklist: []
}

const recipePersistConfig = {
	key: 'auth',
	storage: storage,
	blacklist: []
}

const rootReducer = combineReducers({
	recipes: persistReducer(recipePersistConfig,recipeReducer),
});

export default persistReducer(rootPersistConfig, rootReducer);
