import { GET_RECIPE_PENDING, GET_RECIPE_SUCCESS, GET_RECIPE_ERROR, GET_RECIPE_DETAIL_PENDING,
    GET_RECIPE_DETAIL_ERROR,
    GET_RECIPE_DETAIL_SUCCESS,} from '../actions/recipe.actions';

const INITIAL_STATE = {
    isGetRecipePending: false,
    isGetRecipeSuccess: false,
    getRecipeError: null,
    selectedRecipe: null,
    recipes: [],
    isGetRecipeDetailPending: false,
    isGetRecipeDetailSuccess: false,
    getRecipeDetailError: null,
};

export default function recipeReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_RECIPE_PENDING:
            return { ...state,
                isGetRecipePending: action.isGetRecipePending
            };
        case GET_RECIPE_SUCCESS:
            return { ...state,
                isGetRecipeSuccess: action.isGetRecipeSuccess,
                recipes: action.recipes
            };
        case GET_RECIPE_ERROR:
            return { ...state,
                getRecipeError: action.getRecipeError
            };
        case GET_RECIPE_DETAIL_PENDING:
            return { ...state,
                isGetRecipeDetailPending: action.isGetRecipeDetailPending
            };
        case GET_RECIPE_DETAIL_SUCCESS:
            return { ...state,
                isGetRecipeDetailSuccess: action.isGetRecipeDetailSuccess,
                selectedRecipe: action.selectedRecipe
            };
        case GET_RECIPE_DETAIL_ERROR:
            return { ...state,
                getRecipeDetailError: action.getRecipeDetailError
            };
        default:
            return state;
    }
}

