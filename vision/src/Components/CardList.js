import React from 'react';
import './card.css';
import {Card, CardImage, CardTitle, CardText, CardBody} from 'mdbreact';
import LazyLoad from 'react-lazy-load';
import {Link } from 'react-router-dom';
import {connect} from 'react-redux';
// import photo from '';

class RecipeList extends React.Component {
  handleClick = (idx) => {
    this.props.getRecipeDetails(this.props.recipeState.recipes[idx]);
  }
  renderCards = () => {
    if (this.props.recipeState.recipes)
    return this.props.recipeState.recipes.map( (recipe, idx) => {
      return (
        <LazyLoad key={idx} height={480} offsetTop={50} offsetBotom={50} throtle={100}>
          <Link to={`/recipe/${idx}`} onClick={() => this.handleClick(idx)} >
          <Card  className="animated fadeInLeft w-50 p-3">
            <CardImage className="img-fluid" src={recipe.image} />
            <CardBody>
                <CardTitle>{recipe.title}</CardTitle>
                <CardText className="serving">{recipe.yield}</CardText>
                <CardText className="calories">{recipe.calories}</CardText>
            </CardBody>
          </Card>
          </Link>
        </LazyLoad>
      )
    })
    return null;

  }
  render() {
    return (
      <div className="cardWrapper">
        {this.renderCards()}
      </div>
      
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    recipeState : state.recipes
  }
}
export default connect(mapStateToProps)(RecipeList);