import React from 'react';

const logo = (props) => {
    return(
    <div className="Logo" onClick={props.toggle}>
        <img src={require('./logo.png')} alt="logo"/>
    </div>
    );
}
export default logo;