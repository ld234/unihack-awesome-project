import React,{Component} from 'react';
import Logo from './Logo/Logo';

class sideDrawer extends Component {

  constructor(props){
    super(props);
    this.state={
      glutenFree: false,
      dairyFree: false,
      vegan: false,
      vegetarian: false,
      calories: null,
      minutes: null,
    }
  }
  render(){
  let attachedClasses=["SideDrawer", "Close"];
  let navItems="";
  let appName="";
  if(this.props.open){
    attachedClasses=["SideDrawer", "Open"];
    appName=<h4>VisRecipe</h4>;
    navItems=
     <div>
       <label className="material-checkbox">
        <input type="checkbox"/>
        <span>Vegan</span>
      </label>
      <label className="material-checkbox">
        <input type="checkbox"/>
        <span>Vegetarian</span>
      </label>
      <label className="material-checkbox">
        <input type="checkbox"/>
        <span>glutenFree</span>
      </label>
      <label className="material-checkbox">
        <input type="checkbox"/>
        <span>Dairy Free</span>
      </label>
      <div className="row rangeSlider">
      <label> Calories
        <input type="range" min="0" max="5000"/>
      </label>
      </div>
     </div>;
    
  }
  
  return(
    <div>
      <div className={attachedClasses.join(" ")} >
        <div onClick={this.props.toggleHandler}>
          <Logo/>
        </div>
        {appName}
       {navItems}
      </div>
    </div>
  );
  }
}

export default sideDrawer;
