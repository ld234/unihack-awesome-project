import React, {Component} from 'react';
import {Button} from 'mdbreact';
import {getRecipeDetails} from '../actions/recipe.actions'
import {connect} from 'react-redux';

class RecipeDetailPage extends Component{


    componentDidMount(){
        this.props.getRecipeDetails(this.props.recipeState.recipes[this.props.match.params.id].id);
    }

    render(){
        let imageUrl =this.props.recipeState.selectedRecipe?this.props.recipeState.selectedRecipe.image:"";
        let imgStyle = {
            width: "100%",
            height: "100%",
            backgroundImage: "url(" + imageUrl + ")",
            /* Set a specific height */
            minHeight: "500px", 
            /* Create the parallax scrolling effect */
            backgroundAttachment: "fixed",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
        }

    return(
        
        <div onScroll={this.trackScrolling}>
            <div className="returnButton-container">
                <div href="http://localhost:3000/" className="returnButton">
                    <i className="fas fa-angle-left"></i>
                </div>
            </div>
            <div  id="img" style={imgStyle} ></div>
            <div className="recipeContainer">
                <div className="desc"> 
                    <h2>Recipe Title</h2>
                    <div className="row">
                        <div className="col-sm-6"><h6>CALORIES: {this.props.recipeState.selectedRecipe?this.props.recipeState.selectedRecipe.calories:null}</h6></div>
                        <div className="col-sm-6"><h6>SERVING SIZE: {this.props.recipeState.selectedRecipe?this.props.recipeState.selectedRecipe.servings:null}</h6></div>
                    </div>
                    <h4>INGREDIENTS</h4>
                    <ul>
                    {this.props.recipeState.selectedRecipe&&this.props.recipeState.selectedRecipe.extendedIngredients?this.props.recipeState.selectedRecipe.extendedIngredients.map((item,idx) => {
                        return <li key={idx}>{item.name}</li>
                    }):null}
                    </ul>
                <hr/>
                </div>
                <div className="instruction"> 
                <h5>INSTRUCTIONS</h5>
                <ol>
                    {this.props.recipeState.selectedRecipe&&this.props.recipeState.selectedRecipe.analyzedInstructions?this.props.recipeState.selectedRecipe.analyzedInstructions[0].steps.map((item,idx) => {
                        return <li key={idx}>{item.step}</li>
                    }):null}
                </ol>
            
                </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        recipeState: state.recipes
    }
}

const mapStateToDispatch = (dispatch) => {
    return { 
        getRecipeDetails: (id) => dispatch(getRecipeDetails(id))
    }
}

export default connect(mapStateToProps,mapStateToDispatch)(RecipeDetailPage)