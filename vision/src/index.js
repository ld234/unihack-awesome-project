import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import reduxThunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import persistedReducer from './reducers';
import './index.css';

import App from './App';
import RecipePage from'./Components/RecipeDetailPage';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/mdbreact/dist/css/mdb.css';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

const createStoreWithMiddleware = applyMiddleware(reduxThunk,reduxLogger)(createStore);
const store = createStoreWithMiddleware(persistedReducer);
const persistor = persistStore(store);

ReactDOM.render(
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
				<div id="app">
					<Switch>
						<Route exact path="/" component={App} />
						<Route path="/recipe/:id" component={RecipePage} />
					</Switch>
				</div>
			</BrowserRouter>
		</PersistGate>
	</Provider>
  , document.querySelector('#root'));


